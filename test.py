import requests, urllib2

s = requests.Session()

def make_request_post(url, data, showid):
    try:
        global s
        s.get("https://www.sonyliv.com/details/show/"+showid)
        xsrfToken = s.cookies["XSRF-TOKEN"]
        print s.cookies
        headers = {'Accept': 'application/json, text/plain, */*', 'Content-Type': 'application/json' , 'Connection': 'keep-alive', 'DMAdetails' : '{"message":"SUCCESS","dmaID":"IN","channelPartnerID":"MSMIND","country":"IN","signature":"246dc4ed5fc08c16fe9ccd4709bc5e826ceed6d174fc7ba5a5bf7c7471d7bac5195d9715b968946b04a799619a63c51bed72cf6218d4faf48ff8455b0194443b7132548fa4c0d2ce276d2230be39644835abb1e821788bd144e84046d4c1266de01279d34b6d5ed0c1aacd76cf1f1d1afb13ba9681534637b2dc8ebb23c660bd"}',
                   'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36' , 'x-via-web-device': 'true', 'X-XSRF-TOKEN': xsrfToken}
        response = s.post(url, headers=headers,
                          cookies=s.cookies, data=data, verify=False)
        print response
        data = response.text
        return data
    except urllib2.URLError, e:    # This is the correct syntax
        print e
        # sys.exit(1)

requestPayload = '{"searchSet":[{"type":"search","pageSize":10,"pageNumber":0,"id":"Episodes","sortOrder":"START_DATE:DESC","data":"exact=true&all=type:Episodes&all=showname:taarak mehta ka ooltah chashmah"}],"detailsType":"basic","deviceDetails":{"mfg":"Google Chrome","os":"others","osVer":"XXX","model":"Google Chrome","deviceId":55392572539}}'
html = make_request_post("https://www.sonyliv.com/api/v4/vod/search", requestPayload, "4600324971001")
print html