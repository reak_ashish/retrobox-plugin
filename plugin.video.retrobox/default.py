## Required Includes ##

import re
import os
import sys
import urllib
import urllib2
import json
import requests
import datetime
import time

## XBMC Requirements ##
import xbmc
import xbmcplugin
import xbmcgui
import xbmcaddon
from addon.common.addon import Addon

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

## End of Requirements ##

# Start Python Requests Session

s = requests.Session()
s = requests.Session()
headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Accept-Encoding': 'gzip, deflate, sdch',
           'Connection': 'keep-alive', 'User-Agent': 'AppleCoreMedia/1.0.0.12B411 (iPhone; U; CPU OS 8_1 like Mac OS X; en_gb)'}
s.get("https://www.sonyliv.com/", headers=headers)

## Settings ##
addon_id = 'plugin.video.retrobox'
addon = Addon(addon_id, sys.argv)
Addon = xbmcaddon.Addon(addon_id)
debug = Addon.getSetting('debug')

language = (Addon.getSetting('langType')).lower()
perpage = (Addon.getSetting('perPage'))
moviessortType = (Addon.getSetting('moviessortType')).lower()
enableip = (Addon.getSetting('EnableIP'))
ipaddress = (Addon.getSetting('ipaddress'))
quality = (Addon.getSetting('qualityType')).lower()

dialog = xbmcgui.Dialog()

# Some weird shit that Star plugin used
if moviessortType == 'name':
    moviessortType = 'title+asc'
elif moviessortType == 'newest':
    moviessortType = 'last_broadcast_date+desc'  # ,year+desc,title+asc'
else:
    moviessortType = 'counter+desc'

fold = False


def addon_log(string):
    if debug == 'true':
        xbmc.log("[plugin.video.retrobox-%s]: %s" % (addon_version, string))


def make_request(url):
    try:
        headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8', 'Accept-Encoding': 'gzip, deflate, sdch',
                   'Connection': 'keep-alive', 'User-Agent': 'AppleCoreMedia/1.0.0.12B411 (iPhone; U; CPU OS 8_1 like Mac OS X; en_gb)'}
        response = s.get(url, headers=headers,
                         cookies=s.cookies, verify=False)
        data = response.text
        return data
    except urllib2.URLError, e:    # This is the correct syntax
        addon_log(str(e))
        # sys.exit(1)


def make_request_post(url, data):
    try:
        xsrfToken = s.cookies["XSRF-TOKEN"]
        headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8', 'Content-Type': 'application/json', 'Accept-Encoding': 'gzip, deflate, br', 'Connection': 'keep-alive',
                   'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'X-XSRF-TOKEN': xsrfToken, 'x-via-web-device': 'true'}
        response = s.post(url, headers=headers,
                          cookies=s.cookies, data=data, verify=False)
        data = response.text
        return data
    except urllib2.URLError, e:    # This is the correct syntax
        addon_log(str(e))
        # sys.exit(1)


def m3u8Parser(fileContent):
    videos = []
    matchlist2 = re.compile(
        "BANDWIDTH=([0-9]+)[^\n]*\n([^\n]*)\n").findall(str(fileContent))
    if matchlist2:
        for (size, video) in matchlist2:
            if size:
                size = int(size)
            else:
                size = 0
            videos.append([size, video])
    return videos


def menu():

    # Main Menu Listing

    # Simple format
    # addDir(providerFunction, slug, showURL, imageURL)
    # E.g, addDir("star","showName","https://account.hotstar.com/...","http://media-starag....")

    addDir(7, "mere-angne-mein", "http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=4210&channel=PCTV",
           "http://media0-starag.startv.in/r1/thumbs/PCTV/78/4278/PCTV-4278-hs.jpg", False)

    addDir(7, "dil-boley-oberoi", "http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=12944&channel=PCTV",
           "http://media0-starag.startv.in/r1/thumbs/PCTV/03/13103/PCTV-13103-hs.jpg", False)

    addDir(51, "bhabi-ji-ghar-par-hai", "http://api.android.zeeone.com/mobile/get/show_videos/bhabi-ji-ghar-par-hai/0/150/newest",
           "http://akamai.vidz.zeecdn.com/ozee/shows/bhabi-ji-ghar-par-hai/listing-image-small.jpg", False)

    addDir(7, "ishqbaaaz", "http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=9449&channel=PCTV",
           "http://media0-starag.startv.in/r1/thumbs/PCTV/67/9567/PCTV-9567-hs.jpg", False)

    addDir(51, "bin-kuch-kahe", "http://api.android.zeeone.com/mobile/get/show_videos/bin-kuch-kahe/0/150/newest",
           "http://akamai.vidz.zeecdn.com/ozee/shows/bin-kuch-kahey/listing-image-small.jpg", False)

    addDir(51, "woh-apna-sa", "http://api.android.zeeone.com/mobile/get/show_videos/woh-apna-sa/0/150/newest",
           "http://akamai.vidz.zeecdn.com/ozee/shows/woh-apna-sa/listing-image-small.jpg", False)

    addDir(51, "queens-hain-hum", "http://api.android.zeeone.com/mobile/get/show_videos/queens-hain-hum/0/150/newest",
           "http://akamai.vidz.zeecdn.com/ozee/shows/queens-hain-hum/listing-image-small.jpg", False)

    addDir(61, "pokemon-gotta-catch-em-'all", "https://wapiv2.voot.com/wsv_1_0/media/assetDetails.json?tabId=kidsShows&subTabId=allKidsEpisodes&rowId=915&tvSeriesId=368469&limit=50&offSet=1",
           "https://viacom18-res.cloudinary.com/image/upload/f_auto,q_auto:eco,fl_lossy/kimg/series_cover_image/Pokemon-E.jpg", False)

    addDir(71, 'taarak mehta ka ooltah chashmah', '',
           'http://setindiapd.brightcove.com.edgesuite.net/4338955589001/2017/03/4338955589001_5380327010001_4600324971001-th.jpg', False)

    addDir(24, 'Star Sports', '',
           'https://upload.wikimedia.org/wikipedia/en/0/01/STAR_Sports_Logo_New.jpg', False)

    addDir(101, 'AajTak', '',
           "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", False)


def get_sony_episode(name):
        # Request Payload for Sony
        # {"detailsType":"basic","searchSet":[{"pageSize":15,"pageNumber":0,"sortOrder":"START_DATE:DESC","data":"exact=true&all=type:Episodes&all=showname:taarak mehta ka ooltah chashmah","type":"search","id":"Episodes","itemsUsed":0,"language":""}],"deviceDetails":{"mfg":"Google Chrome","os":"others","osVer":"XXX","model":"Google Chrome","deviceId":77128084247}}
    requestPayload = '{"detailsType":"basic","searchSet":[{"pageSize":30,"pageNumber":0,"sortOrder":"START_DATE:DESC","data":"exact=true&all=type:Episodes&all=showname:'+name + \
        '","type":"search","id":"Episodes","itemsUsed":0,"language":""}],"deviceDetails":{"mfg":"Google Chrome","os":"others","osVer":"XXX","model":"Google Chrome","deviceId":77128084247}}'
    html = make_request_post(
        "https://www.sonyliv.com/api/v4/vod/search", requestPayload)
    data = json.loads(html)
    for var in data[0]["assets"]:
        addDir(72, var["title"], var["hlsUrl"], var["thumbnailUrl"], False)


def play_sony_episode(url, image):
    html = make_request(url)
    data = html.splitlines()
    addDir(0, "[144] "+name, data[3], image, isplayable=True)
    addDir(0, "[240] "+name, data[7], image, isplayable=True)
    addDir(0, "[360] "+name, data[13], image, isplayable=True)
    addDir(0, "[540] "+name, data[15], image, isplayable=True)
    addDir(0, "[720] "+name, data[19], image, isplayable=True)


def ozee_get_episode():
    html = make_request(url)
    data = json.loads(html)
    for var in data:
        try:
            name = var['video_title']
            slug = var['slug']
            slug = "http://api.android.zeeone.com/mobile/get/show_video/"+slug
            image = var['video_image']
            addDir(52, name, slug, image, False)
        except:
            print "error"


def ozee_play_episode():
    html = make_request(url)
    data = json.loads(html)
    name = data['title']
    playbackurl = data['playback_url']
    preurl = playbackurl.split("master")
    image = data['listing_image_small']
    html = make_request(playbackurl)
    links = html.splitlines()
    addDir(0, "[144] "+name, preurl[0]+links[4], image, isplayable=True)
    addDir(0, "[240] "+name, preurl[0]+links[6], image, isplayable=True)
    addDir(0, "[480] "+name, preurl[0]+links[8], image, isplayable=True)


def voot_get_episode(url):
    html = make_request(url)
    bobo = json.loads(html)
    for var in bobo["assets"][0]["items"]:
        name = var['title']+" S"+var["season"]+" E"+var["episodeNo"]
        url = 'https://wapiv2.voot.com/wsv_1_0/playback.json?limit=10&offSet=0&mediaId=' + \
            var["mId"]
        image = var["imgURL"]
        addDir(62, name, url, image, False)


def voot_play_episode(url):
    data = make_request(url)
    data = json.loads(data)
    data = data["assets"][0]["assets"][0]["items"][0]
    showData = data
    playbackUrl = data["URL"]
    data = make_request(playbackUrl)
    urls = m3u8Parser(data)
    for url in urls:
        addDir(0, "["+str(url[0])+"] - "+showData["mediaName"],
               url[1], showData["imgURL"], isplayable=True)


def play_aajtak():
    #addDir(0, "[144] "+name, preurl[0]+links[4], image, isplayable=True)
    #addDir(0, "[240] "+name, preurl[0]+links[6], image, isplayable=True)
    secrettoken = make_request(
        'http://vidgyor.com/prod/aajtak/aajtak_auth.json')
    secrettoken = json.loads(secrettoken)
    secrettoken = secrettoken['token']
    addDir(0, "[144] AajTak Live News", "http://atcdn.vidgyor.com/at-origin/live5/chunks.m3u8" +
           secrettoken, "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", isplayable=True)
    addDir(0, "[240] AajTak Live News", "http://atcdn.vidgyor.com/at-origin/live4/chunks.m3u8" +
           secrettoken, "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", isplayable=True)
    addDir(0, "[360] AajTak Live News", "http://atcdn.vidgyor.com/at-origin/live3/chunks.m3u8" +
           secrettoken, "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", isplayable=True)
    addDir(0, "[480] AajTak Live News", "http://atcdn.vidgyor.com/at-origin/live2/chunks.m3u8" +
           secrettoken, "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", isplayable=True)
    addDir(0, "[720] AajTak Live News", "http://atcdn.vidgyor.com/at-origin/live1/chunks.m3u8" +
           secrettoken, "http://media2.intoday.in/aajtak/1.0.2/resources/images/logo.jpg", isplayable=True)


# Helper Functions

def addDir(mode, name, url, image, duration="", isplayable=False):
    name = name.encode('utf-8', 'ignore')
    namenew = name.replace("-", " ")
    namenew = namenew.title()
    url = url.encode('utf-8', 'ignore')
    #image = image.encode('utf-8', 'ignore')

    if 0 == mode:
        link = url
        print link
    else:
        link = sys.argv[0]+"?mode="+str(mode)+"&name="+urllib.quote_plus(
            name)+"&url="+urllib.quote_plus(url)+"&image="+urllib.quote_plus(image)

    ok = True
    item = xbmcgui.ListItem(
        namenew, iconImage="DefaultFolder.png", thumbnailImage=image)
    item.setInfo(type="Video", infoLabels={
                 "Title": namenew, "Duration": duration})
    item.setArt({'fanart': image})
    ## Adjust for OZEE systems - Search for listing jpg and replace fanart with other ##
    if 'vl.jpg' in image:
        image2 = image.replace('vl.jpg', 'hl.jpg')
        item.setArt({'fanart': image2})
    else:
        item.setArt({'fanart': image})
    if 'listing-image-small.jpg' in image:
        ## Adjust for OZEE systems - Search for listing jpg and replace fanart with other ##
        image2 = image.replace('listing-image-small.jpg', 'feature-image.jpg')
        item.setArt({'fanart': image2})
    else:
        item.setArt({'fanart': image})

    isfolder = True
    if isplayable:
        item.setProperty('IsPlayable', 'true')
        isfolder = False
    ok = xbmcplugin.addDirectoryItem(handle=int(
        sys.argv[1]), url=link, listitem=item, isFolder=isfolder)
    return ok


def get_params():
    param = []
    paramstring = sys.argv[2]
    if len(paramstring) >= 2:
        params = sys.argv[2]
        cleanedparams = params.replace('?', '')
        if (params[len(params)-1] == '/'):
            params = params[0:len(params)-2]
        pairsofparams = cleanedparams.split('&')
        param = {}
        for i in range(len(pairsofparams)):
            splitparams = {}
            splitparams = pairsofparams[i].split('=')
            if (len(splitparams)) == 2:
                param[splitparams[0]] = splitparams[1]
    return param


# Star Rubbish, Carried from old repo

def get_seasons():
        #print url
    html = make_request(url)
    #print html
    #data = html.decode('utf-8')
    data = html
    html = json.loads(data)
    for result in html['resultObj']['contentInfo']:
        seasons = result['contentTitle']
        # 'http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId='++'&channel=PCTV'#
        season_link = 'http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=' + \
            str(result['categoryId'])+'&channel=PCTV'
        # 'http://media0-starag.startv.in/r1/thumbs/PCTV/'+str(result['urlPictures'])[-2:]+'/'+str(result['urlPictures'])+'/PCTV-'+str(result['urlPictures'])+'-hcc.jpg'
        season_img = ''
        addDir(7, seasons, season_link, season_img, False)

    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_LABEL)


def get_seasons_ep_links():
    html = make_request(url)
    data = html
    html = json.loads(data)
    for result in html['resultObj']['categoryList']:
        for result2 in result['categoryList']:
            ep_titles = result2['categoryName']
            ep_links = 'http://account.hotstar.com/AVS/besc?action=GetArrayContentList&categoryId=' + \
                str(result2['categoryId'])+'&channel=PCTV'
            ep_images = 'http://media0-starag.startv.in/r1/thumbs/PCTV/' + \
                str(result2['urlPictures'])[-2:]+'/'+str(result2['urlPictures']
                                                         )+'/PCTV-'+str(result2['urlPictures'])+'-hsea.jpg'
            addDir(8, ep_titles, ep_links, ep_images, False)

    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_LABEL)
    setView('episodes', 'episode-view')


def get_episodes():
    html = make_request(url)
    #data = html.decode('utf-8')
    data = html
    html = json.loads(data)
    for result in html['resultObj']['contentList']:
        fin_ep_titles = str(result['episodeNumber'])+' - ' + \
            result['episodeTitle'].encode('ascii', 'ignore')
        duration = result['duration']
        fin_ep_links = 'http://agl-intl.hotstar.com/AVS/besc?action=GetCDN&asJson=Y&channel=PCTV&id=' + \
            str(result['contentId'])+'&type=VOD'
        fin_ep_images = 'http://media0-starag.startv.in/r1/thumbs/PCTV/' + \
            str(result['urlPictures'])[-2:]+'/'+str(result['urlPictures']
                                                    )+'/PCTV-'+str(result['urlPictures'])+'-hs.jpg'
        addDir(9, fin_ep_titles, fin_ep_links, fin_ep_images, duration, fold)

    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_LABEL)
    setView('episodes', 'episode-view')


def get_new_sports():
    html = make_request(
        'http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=5962&channel=PCTV')
    data = html
    html = json.loads(data)
    for result in html['resultObj']['categoryList'][0]['categoryList']:
        if '5967' not in str(result['categoryId']):
            if 'Popular in' not in result['contentTitle']:
                if 'masthead' in result['contentTitle']:
                    title = 'Live Events (Praise the god and click)'
                else:
                    title = result['contentTitle']
                sports_link = 'http://account.hotstar.com/AVS/besc?action=GetArrayContentList&categoryId=' + \
                    str(result['categoryId'])+'&channel=PCTV'
                sports_img = ''
                addDir(14, title, sports_link, sports_img, False)

    xbmcplugin.endOfDirectory(int(sys.argv[1]), cacheToDisc=True)


def get_ss_event():
    # print 'ss url', url
    html = make_request(url)
    data = html
    html = json.loads(data)
    if 'ArrayContent' in url:
        for result in html['resultObj']['contentList']:
            title = result['contentTitle']
            duration = result['duration']
            event_link = 'http://getcdn.hotstar.com/AVS/besc?action=GetCDN&asJson=Y&channel=PCTV&id=' + \
                str(result['contentId'])+'&type=VOD'
            event_img = 'http://media0-starag.startv.in/r1/thumbs/PCTV/' + \
                str(result['urlPictures'])[-2:]+'/'+str(result['urlPictures']
                                                        )+'/PCTV-'+str(result['urlPictures'])+'-hl.jpg'
            addDir(9, title, event_link, event_img, duration, fold)
    else:
        for result in html['resultObj']['categoryList'][0]['categoryList']:
            title = result['contentTitle']
            event_link = 'http://account.hotstar.com/AVS/besc?action=GetArrayContentList&categoryId=' + \
                str(result['categoryId'])+'&channel=PCTV'
            event_img = ''
            addDir(14, title, event_link, event_img, False)

    setView('episodes', 'episode-view')
    xbmcplugin.endOfDirectory(int(sys.argv[1]), cacheToDisc=True)


def get_collections():
    html = make_request(
        'http://account.hotstar.com/AVS/besc?action=GetCatalogueTree&categoryId=558&channel=PCTV')
    data = html
    html = json.loads(data)
    for result in html['resultObj']['categoryList']:
        for subval in result['categoryList']:
            if language in subval['language'].lower():
                title = '[B][COLOR green]' + \
                    subval['categoryName']+'[/COLOR][/B]'
                col_link = 'http://account.hotstar.com/AVS/besc?action=GetArrayContentList&categoryId=' + \
                    str(subval['categoryId'])+'&channel=PCTV'
                col_img = 'http://media0-starag.startv.in/r1/thumbs/PCTV/' + \
                    str(result['urlPictures'])[-2:]+'/'+str(result['urlPictures']
                                                            )+'/PCTV-'+str(result['urlPictures'])+'-hs.jpg'
                addDir(10, title, col_link, col_img, False)

    xbmcplugin.endOfDirectory(int(sys.argv[1]), cacheToDisc=True)


def get_video_url():
    videos = []
    params = []
    html = make_request(url)
    # name = 'testing singham returns'
    # if 'testing' in name:
    # image = ''
    data = html
    html = json.loads(data)
    manifest1 = html['resultObj']['src']
    manifest1 = manifest1.replace('http://', 'https://')
    manifest1 = manifest1.replace('/z/', '/i/')
    manifest1 = manifest1.replace('manifest.f4m', 'master.m3u8')
    if manifest1:
        manifest_url = make_request(manifest1)
        if manifest_url:
            matchlist2 = re.compile(
                "BANDWIDTH=([0-9]+)[^\n]*\n([^\n]*)\n").findall(str(manifest_url))
            if matchlist2:
                for (size, video) in matchlist2:
                    if size:
                        size = int(size)
                    else:
                        size = 0
                    videos.append([size, video])
        else:
            videos.append([-2, match])
    videos.sort(key=lambda L: L and L[0], reverse=True)
    cookieString = ""
    c = s.cookies
    i = c.items()
    for name2, value in i:
        cookieString += name2 + "=" + value + ";"
    # print 'cookieString is', cookieString
    for video in videos:
        size = '[' + str(video[0]) + '] '
        # print 'video 1 is', video[1]
        # print 'size is', size
        # print 'name is', name
        # print 'ipaddress is', ipaddress
        # print 'image is', image
        if enableip == 'true':
            addDir(0, size + name, video[1]+"|Cookie="+cookieString +
                   "&X-Forwarded-For="+ipaddress, image, isplayable=True)
        else:
            addDir(0, size + name, video[1]+"|Cookie=" +
                   cookieString, image, isplayable=True)

    setView('movies', 'movie-view')


def setView(content, viewType):

    if content:
        xbmcplugin.setContent(int(sys.argv[1]), content)

    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_UNSORTED)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_LABEL)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_VIDEO_RATING)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_DATE)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_PROGRAM_COUNT)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_VIDEO_RUNTIME)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_GENRE)
    xbmcplugin.addSortMethod(handle=int(
        sys.argv[1]), sortMethod=xbmcplugin.SORT_METHOD_MPAA_RATING)

# Rubbish that should have been refactored


params = get_params()
mode = None
name = None
url = None
image = None

try:
    mode = int(params["mode"])
except:
    pass
try:
    name = urllib.unquote_plus(params["name"])
except:
    pass
try:
    url = urllib.unquote_plus(params["url"])
except:
    pass
try:
    image = urllib.unquote_plus(params["image"])
except:
    pass

if mode == None:
    menu()

if mode == 3:
    print "test"
    # Insert bhabhiji function

if mode == 6:
    # get_seasons()
    get_seasons_ep_links()

if mode == 7:
    get_seasons_ep_links()

if mode == 8:
    get_episodes()

if mode == 9:
    get_video_url()

if mode == 51:
    ozee_get_episode()

if mode == 52:
    ozee_play_episode()

if mode == 61:
    voot_get_episode(url)

if mode == 62:
    voot_play_episode(url)

if mode == 71:
    get_sony_episode(name)

if mode == 72:
    play_sony_episode(url, image)

if mode == 14:
    get_ss_event()

if mode == 24:
    get_new_sports()

if mode == 101:
    play_aajtak()

s.close()

xbmcplugin.endOfDirectory(int(sys.argv[1]))
